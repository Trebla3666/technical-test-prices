FROM eclipse-temurin:21-jdk-alpine
VOLUME /tmp
COPY boot/target/*.jar app.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=standalone", "-jar", "/app.jar"]