package com.inditex.challenge.domain.model;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * Value Object representing a money amount.
 */
@Getter
@Builder
@ToString
public class Amount {

  // Quantity of the money amount
  @NotNull
  BigDecimal value;

  // Currency of the money amount according to ISO 4217
  @NotEmpty
  String currency;

}
