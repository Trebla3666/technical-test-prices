package com.inditex.challenge.domain.exceptions;

/**
 * Exception representing that a Price has already been decommissioned.
 */
public class AlreadyDecommissionedException extends RuntimeException {

  private static final String MESSAGE = "The price with public id '%s' is already decommissioned";

  public AlreadyDecommissionedException(String publicId) {
    super(String.format(MESSAGE, publicId));
  }
}
