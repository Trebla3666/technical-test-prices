package com.inditex.challenge.domain.model;

/**
 * Possible values for the status of a price.
 */
public enum PriceStatus {
  APPLICABLE,
  DECOMMISSIONED
}
