package com.inditex.challenge.domain.services;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.Product;

/**
 * Repository service to operate with the Price entity.
 */
public interface ProductService {

  void addPrice(Product product, Price price);

}
