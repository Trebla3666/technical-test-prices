package com.inditex.challenge.domain.model;

import com.inditex.challenge.domain.exceptions.AlreadyDecommissionedException;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.ToString;

/**
 * Domain entity representing the Price for a Product.
 */
@Getter
@Builder
@ToString
public class Price {

  // Internal unique identifier of the price
  Long id;

  // Public unique identifier of the price
  @Default
  String publicId = UUID.randomUUID().toString();

  // Unique identifier of the fee associated to the price
  @NotEmpty
  String priceListId;

  // Start datetime when the price is applicable
  @NotNull
  Instant startDate;

  // End datetime when the price is applicable
  @NotNull
  Instant endDate;

  // Priority to aid in disambiguation
  int priority;

  // Money amount representing the actual price
  @NotNull
  Amount amount;

  // Status of the price
  @Default
  PriceStatus status = PriceStatus.APPLICABLE;

  /**
   * Logically deletes the price.
   */
  public void decommission() {
    if (PriceStatus.DECOMMISSIONED.equals(status)) {
      throw new AlreadyDecommissionedException(publicId);
    }
    status = PriceStatus.DECOMMISSIONED;
  }

  /**
   * Replaces the Price data with the provided one.
   *
   * @param price to be merged into this one.
   */
  public void merge(Price price) {
    publicId = price.getPublicId();
    priceListId = price.getPriceListId();
    startDate = price.getStartDate();
    endDate = price.getEndDate();
    priority = price.getPriority();
    amount = price.getAmount();
    status = price.getStatus();
  }

}
