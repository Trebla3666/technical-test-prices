package com.inditex.challenge.domain.model;

import jakarta.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.ToString;

/**
 * Domain Aggregate Root representing a Product.
 */
@Getter
@Builder
@ToString
public class Product {

  // Unique identifier of the product
  @NotEmpty
  String productId;

  // Unique identifier of the brand
  @NotEmpty
  String brandId;

  // List of prices applicable to the product
  @Default
  List<Price> prices = new ArrayList<>();

}
