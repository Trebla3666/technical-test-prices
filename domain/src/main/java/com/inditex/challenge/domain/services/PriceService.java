package com.inditex.challenge.domain.services;

import com.inditex.challenge.domain.model.Price;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Repository service to operate with the Price entity.
 */
public interface PriceService {

  Optional<Price> findApplicablePrice(Instant applicableDate, String productId, String brandId);

  Optional<Price> findPrice(String publicId, String productId, String brandId);

  List<Price> findPrices(String productId, String brandId);

  void updatePrice(Price price, String productId, String brandId);
}
