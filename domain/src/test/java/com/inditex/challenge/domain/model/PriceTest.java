package com.inditex.challenge.domain.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.Test;

public class PriceTest {

  @Test
  void price_decommission() {
    // Given
    Price price = Price.builder().build();

    // When
    price.decommission();

    // Then
    assertThat(price.getStatus()).isEqualTo(PriceStatus.DECOMMISSIONED);
  }

  @Test
  void price_merge() {
    // Given
    Price price = Price.builder().build();
    Price toBeMerged = price().build();

    // When
    price.merge(toBeMerged);

    // Then
    assertThat(price)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(toBeMerged);
  }

  private Price.PriceBuilder price() {
    return Price.builder()
        .publicId("a_public_id")
        .priceListId("a_price_list_id")
        .status(PriceStatus.APPLICABLE)
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(Amount.builder()
            .value(new BigDecimal("10.75"))
            .currency("EUR")
            .build());
  }

}
