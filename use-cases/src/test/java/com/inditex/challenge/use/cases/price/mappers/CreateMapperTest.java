package com.inditex.challenge.use.cases.price.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import com.inditex.challenge.domain.model.Amount;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.PriceStatus;
import com.inditex.challenge.domain.model.Product;
import com.inditex.challenge.use.cases.price.dtos.CreateRequestDto;
import com.inditex.challenge.use.cases.price.dtos.CreateResponseDto;
import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class CreateMapperTest {

  CreateMapper mapper = Mappers.getMapper(CreateMapper.class);

  @Test
  void create_request_to_product() {
    // Given
    CreateRequestDto origin = createRequestDto().build();

    // When
    final Product target = mapper.mapProduct(origin);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(product().build());
  }

  @Test
  void create_request_to_price() {
    // Given
    CreateRequestDto origin = createRequestDto().build();

    // When
    final Price target = mapper.map(origin);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .ignoringFields("publicId")
        .isEqualTo(price().build());
  }

  @Test
  void price_to_create_response() {
    // Given
    Price origin = price().build();

    // When
    final CreateResponseDto target = mapper.map(origin);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(createResponseDto().build());
  }

  private Product.ProductBuilder product() {
    return Product.builder()
        .productId("a_product_id")
        .brandId("a_brand_id");
  }

  private Price.PriceBuilder price() {
    return Price.builder()
        .publicId("a_public_id")
        .priceListId("a_price_list_id")
        .status(PriceStatus.APPLICABLE)
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(Amount.builder()
            .value(new BigDecimal("10.75"))
            .currency("EUR")
            .build());
  }

  private CreateRequestDto.CreateRequestDtoBuilder createRequestDto() {
    return CreateRequestDto.builder()
        .productId("a_product_id")
        .brandId("a_brand_id")
        .priceListId("a_price_list_id")
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(new BigDecimal("10.75"))
        .currency("EUR");
  }

  private CreateResponseDto.CreateResponseDtoBuilder createResponseDto() {
    return CreateResponseDto.builder()
        .publicId("a_public_id")
        .status("APPLICABLE");
  }

}
