package com.inditex.challenge.use.cases.price.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import com.inditex.challenge.domain.model.Amount;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.PriceStatus;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class GetPriceMapperTest {

  GetPriceMapper mapper = Mappers.getMapper(GetPriceMapper.class);

  @Test
  void price_to_get_response() {
    // Given
    Price origin = price().build();

    // When
    final GetPriceResponseDto target = mapper.map(origin);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(getPriceResponseDto().build());
  }

  @Test
  void price_list_to_get_response_list() {
    // Given
    List<Price> origin = List.of(price().build(), price().publicId("other_id").build());

    // When
    final List<GetPriceResponseDto> target = mapper.map(origin);

    // Then
    assertThat(target).isNotNull();
    assertThat(target).hasSize(2);
  }

  private Price.PriceBuilder price() {
    return Price.builder()
        .publicId("a_public_id")
        .priceListId("a_price_list_id")
        .status(PriceStatus.APPLICABLE)
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(Amount.builder()
            .value(new BigDecimal("10.75"))
            .currency("EUR")
            .build());
  }

  private GetPriceResponseDto.GetPriceResponseDtoBuilder getPriceResponseDto() {
    return GetPriceResponseDto.builder()
        .publicId("a_public_id")
        .status("APPLICABLE")
        .priceListId("a_price_list_id")
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(new BigDecimal("10.75"))
        .currency("EUR");
  }

}
