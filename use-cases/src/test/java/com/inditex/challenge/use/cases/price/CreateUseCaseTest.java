package com.inditex.challenge.use.cases.price;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.Product;
import com.inditex.challenge.domain.services.ProductService;
import com.inditex.challenge.use.cases.price.dtos.CreateRequestDto;
import com.inditex.challenge.use.cases.price.dtos.CreateResponseDto;
import com.inditex.challenge.use.cases.price.mappers.CreateMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CreateUseCaseTest {

  @Mock
  ProductService productService;

  @Mock
  CreateMapper createMapper;

  @InjectMocks
  CreateUseCase useCase;

  @Test
  void create_use_case() {
    // Given
    CreateRequestDto requestDto = CreateRequestDto.builder().build();

    Price price = Price.builder().build();
    when(createMapper.map(requestDto)).thenReturn(price);
    Product product = Product.builder().build();
    when(createMapper.mapProduct(requestDto)).thenReturn(product);

    CreateResponseDto responseDto = CreateResponseDto.builder().build();
    when(createMapper.map(price)).thenReturn(responseDto);

    // When
    CreateResponseDto result = useCase.execute(requestDto);

    // Then
    assertThat(result).isEqualTo(responseDto);
  }
}
