package com.inditex.challenge.use.cases.price;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import com.inditex.challenge.use.cases.price.mappers.GetPriceMapper;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GetApplicablePriceUseCaseTest {

  @Mock
  PriceService priceService;

  @Mock
  GetPriceMapper getPriceMapper;

  @InjectMocks
  GetApplicablePriceUseCase useCase;

  @Test
  void get_applicable_price_use_case_ok() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    Instant applicableDate = Instant.ofEpochSecond(10);

    Price maxPrice = Price.builder().priority(10).build();
    when(priceService.findApplicablePrice(applicableDate, productId, brandId))
        .thenReturn(Optional.of(maxPrice));

    GetPriceResponseDto mapped = GetPriceResponseDto.builder().build();
    when(getPriceMapper.map(maxPrice)).thenReturn(mapped);

    // When
    GetPriceResponseDto responseDto = useCase.execute(productId, brandId, applicableDate);

    // Then
    assertThat(responseDto).isEqualTo(mapped);
  }

  @Test
  void get_applicable_price_use_case_not_found() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    Instant applicableDate = Instant.ofEpochSecond(10);

    when(priceService.findApplicablePrice(applicableDate, productId, brandId))
        .thenReturn(Optional.empty());

    // When
    Throwable throwable = catchThrowable(() -> useCase.execute(productId, brandId, applicableDate));

    // Then
    assertThat(throwable).isNotNull();
    assertThat(throwable).isInstanceOf(NotFoundException.class);
  }
}
