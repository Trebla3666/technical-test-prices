package com.inditex.challenge.use.cases.price.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import com.inditex.challenge.domain.model.Amount;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.PriceStatus;
import com.inditex.challenge.use.cases.price.dtos.UpdateRequestDto;
import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class UpdateMapperTest {

  UpdateMapper mapper = Mappers.getMapper(UpdateMapper.class);

  @Test
  void update_request_to_price() {
    // Given
    UpdateRequestDto origin = updateRequestDto().build();

    // When
    final Price target = mapper.map(origin);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(price().build());
  }

  private Price.PriceBuilder price() {
    return Price.builder()
        .publicId("a_public_id")
        .priceListId("a_price_list_id")
        .status(PriceStatus.APPLICABLE)
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(Amount.builder()
            .value(new BigDecimal("10.75"))
            .currency("EUR")
            .build());
  }

  private UpdateRequestDto.UpdateRequestDtoBuilder updateRequestDto() {
    return UpdateRequestDto.builder()
        .priceId("a_public_id")
        .priceListId("a_price_list_id")
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(new BigDecimal("10.75"))
        .currency("EUR");
  }

}
