package com.inditex.challenge.use.cases.price;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DeleteUseCaseTest {

  @Mock
  PriceService priceService;

  @InjectMocks
  DeleteUseCase useCase;

  @Test
  void delete_use_case_ok() {
    // Given
    String priceId = "a_price_id";
    String productId = "a_product_id";
    String brandId = "a_brand_id";

    Price price = spy(Price.builder().build());
    when(priceService.findPrice(priceId, productId, brandId)).thenReturn(Optional.of(price));

    // When
    useCase.execute(priceId, productId, brandId);

    // Then
    verify(price).decommission();
    verify(priceService).updatePrice(price, productId, brandId);
  }

  @Test
  void delete_use_case_not_found() {
    // Given
    String priceId = "a_price_id";
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    when(priceService.findPrice(priceId, productId, brandId)).thenReturn(Optional.empty());

    // When
    Throwable throwable = catchThrowable(() -> useCase.execute(priceId, productId, brandId));

    // Then
    assertThat(throwable).isNotNull();
    assertThat(throwable).isInstanceOf(NotFoundException.class);
  }
}
