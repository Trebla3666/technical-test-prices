package com.inditex.challenge.use.cases.price;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.mappers.GetPriceMapper;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GetPricesUseCaseTest {

  @Mock
  PriceService priceService;

  @Mock
  GetPriceMapper getPriceMapper;

  @InjectMocks
  GetPricesUseCase useCase;

  @Test
  void get_prices_use_case_ok() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";

    List<Price> prices = List.of(Price.builder().build());
    when(priceService.findPrices(productId, brandId)).thenReturn(prices);

    List<GetPriceResponseDto> mapped = List.of(GetPriceResponseDto.builder().build());
    when(getPriceMapper.map(prices)).thenReturn(mapped);

    // When
    List<GetPriceResponseDto> response = useCase.execute(productId, brandId);

    // Then
    assertThat(response).isEqualTo(mapped);
  }
}
