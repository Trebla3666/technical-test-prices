package com.inditex.challenge.use.cases.price.mappers;

import com.inditex.challenge.domain.model.Amount;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.Product;
import com.inditex.challenge.use.cases.price.dtos.CreateRequestDto;
import com.inditex.challenge.use.cases.price.dtos.CreateResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapping related to the Creation Use Case.
 */
@Mapper
public interface CreateMapper {

  @Mapping(target = "prices", ignore = true)
  Product mapProduct(CreateRequestDto dto);

  @Mapping(target = "amount", source = ".")
  @Mapping(target = "id", ignore = true)
  @Mapping(target = "publicId", ignore = true)
  @Mapping(target = "status", ignore = true)
  Price map(CreateRequestDto dto);

  @Mapping(target = "value", source = "amount")
  Amount mapAmount(CreateRequestDto dto);

  CreateResponseDto map(Price price);

}
