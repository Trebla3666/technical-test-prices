package com.inditex.challenge.use.cases.price;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import com.inditex.challenge.use.cases.price.mappers.GetPriceMapper;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Use Case for retrieving a Price by its publicId.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class GetPriceUseCase {

  private final PriceService priceService;

  private final GetPriceMapper getPriceMapper;

  /**
   * Get the related information of a Price.
   *
   * @param priceId Identifier of the Price.
   * @param productId Identifier of the Product.
   * @param brandId Identifier of the Brand.
   * @return GetPriceResponseDto with the associated information of the Price.
   */
  public GetPriceResponseDto execute(@NotNull String priceId, @NotNull String productId,
      @NotNull String brandId) {
    log.debug("execute: {}, {}, {}", priceId, productId, brandId);

    Price price = priceService.findPrice(priceId, productId, brandId)
        .orElseThrow(() -> new NotFoundException(priceId));

    return getPriceMapper.map(price);
  }

}
