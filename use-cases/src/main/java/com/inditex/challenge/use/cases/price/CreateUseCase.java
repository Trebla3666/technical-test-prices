package com.inditex.challenge.use.cases.price;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.Product;
import com.inditex.challenge.domain.services.ProductService;
import com.inditex.challenge.use.cases.price.dtos.CreateRequestDto;
import com.inditex.challenge.use.cases.price.dtos.CreateResponseDto;
import com.inditex.challenge.use.cases.price.mappers.CreateMapper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Use Case for Price Creation.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class CreateUseCase {

  private final ProductService productService;

  private final CreateMapper createMapper;

  /**
   * Creates a Price for a Product.
   *
   * @param requestDto Necessary data for the creation of the Price.
   * @return CreateResponseDto with the publicId and the status of the created Price.
   */
  public CreateResponseDto execute(@Valid CreateRequestDto requestDto) {
    log.debug("execute: {}", requestDto);

    Product product = createMapper.mapProduct(requestDto);
    Price price = createMapper.map(requestDto);

    productService.addPrice(product, price);

    return createMapper.map(price);
  }
}
