package com.inditex.challenge.use.cases.price.dtos;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class CreateResponseDto {

  @NotEmpty
  String publicId;

  @NotEmpty
  String status;

}
