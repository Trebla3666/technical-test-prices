package com.inditex.challenge.use.cases.price;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.use.cases.price.dtos.UpdateRequestDto;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import com.inditex.challenge.use.cases.price.mappers.UpdateMapper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Use Case for Price Update.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class UpdateUseCase {

  private final PriceService priceService;

  private final UpdateMapper updateMapper;

  /**
   * Updates a Price.
   *
   * @param requestDto Necessary data for the update of the Price.
   */
  public void execute(@Valid UpdateRequestDto requestDto) {
    log.debug("execute: {}", requestDto);

    String priceId = requestDto.getPriceId();
    Price price = priceService.findPrice(priceId, requestDto.getProductId(),
            requestDto.getBrandId())
        .orElseThrow(() -> new NotFoundException(priceId));

    price.merge(updateMapper.map(requestDto));

    priceService.updatePrice(price, requestDto.getProductId(), requestDto.getBrandId());
  }
}
