package com.inditex.challenge.use.cases.price.mappers;

import com.inditex.challenge.domain.model.Amount;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.use.cases.price.dtos.UpdateRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapping related to the Update Use Case.
 */
@Mapper
public interface UpdateMapper {

  @Mapping(target = "amount", source = ".")
  @Mapping(target = "publicId", source = "priceId")
  @Mapping(target = "id", ignore = true)
  @Mapping(target = "status", ignore = true)
  Price map(UpdateRequestDto dto);

  @Mapping(target = "value", source = "amount")
  Amount mapAmount(UpdateRequestDto dto);

}
