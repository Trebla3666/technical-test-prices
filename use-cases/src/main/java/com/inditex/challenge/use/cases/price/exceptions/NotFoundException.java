package com.inditex.challenge.use.cases.price.exceptions;

/**
 * Exception representing that an entity was not found.
 */
public class NotFoundException extends RuntimeException {

  private static final String MESSAGE = "Entity was not found.";

  private static final String MESSAGE_ID = "The entity with public id '%s' was not found.";

  public NotFoundException() {
    super(String.format(MESSAGE));
  }

  public NotFoundException(String publicId) {
    super(String.format(MESSAGE_ID, publicId));
  }
}
