package com.inditex.challenge.use.cases.price;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Use Case for Price Deletion.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class DeleteUseCase {

  private final PriceService priceService;

  /**
   * Deletes a Price.
   *
   * @param priceId Identifier of the Price.
   * @param productId Identifier of the Product.
   * @param brandId Identifier of the Brand.
   */
  public void execute(@NotNull String priceId, @NotNull String productId, @NotNull String brandId) {
    log.debug("execute: {}, {}, {}", priceId, productId, brandId);

    Price price = priceService.findPrice(priceId, productId, brandId)
        .orElseThrow(() -> new NotFoundException(priceId));

    price.decommission();

    priceService.updatePrice(price, productId, brandId);
  }

}
