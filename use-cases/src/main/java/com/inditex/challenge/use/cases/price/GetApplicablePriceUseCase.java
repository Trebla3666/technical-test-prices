package com.inditex.challenge.use.cases.price;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import com.inditex.challenge.use.cases.price.mappers.GetPriceMapper;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Use Case for retrieving the applicable Price for a Product at a specific moment.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class GetApplicablePriceUseCase {

  private final PriceService priceService;

  private final GetPriceMapper getPriceMapper;

  /**
   * Get the applicable Price for a Product given some requisites.
   *
   * @param productId Identifier of the Product.
   * @param brandId Identifier of the Brand.
   * @param applicableDate Date of applicability for the Price.
   * @return GetPriceResponseDto with the associated information of the Price.
   */
  public GetPriceResponseDto execute(@NotNull String productId, @NotNull String brandId,
      @NotNull Instant applicableDate) {
    log.debug("execute: {}, {}, {}", productId, brandId, applicableDate);

    Price price = priceService.findApplicablePrice(applicableDate, productId, brandId)
        .orElseThrow(NotFoundException::new);

    return getPriceMapper.map(price);
  }
}
