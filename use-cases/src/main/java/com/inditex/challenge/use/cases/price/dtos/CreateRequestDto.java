package com.inditex.challenge.use.cases.price.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class CreateRequestDto {

  @NotEmpty
  String productId;

  @NotEmpty
  String brandId;

  @NotEmpty
  String priceListId;

  @NotNull
  Instant startDate;

  @NotNull
  Instant endDate;

  int priority;

  @NotNull
  BigDecimal amount;

  @NotEmpty
  String currency;

}
