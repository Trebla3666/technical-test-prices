package com.inditex.challenge.use.cases.price.mappers;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapping related to the Get Price Use Case.
 */
@Mapper
public interface GetPriceMapper {

  @Mapping(target = "amount", source = "amount.value")
  @Mapping(target = "currency", source = "amount.currency")
  GetPriceResponseDto map(Price price);

  List<GetPriceResponseDto> map(List<Price> prices);

}
