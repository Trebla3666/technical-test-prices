package com.inditex.challenge.use.cases.price;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.mappers.GetPriceMapper;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Use Case for retrieving the Prices of a Product.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class GetPricesUseCase {

  private final PriceService priceService;

  private final GetPriceMapper getPriceMapper;

  /**
   * Get the list of Prices for a Product given some requisites.
   *
   * @param productId Identifier of the Product.
   * @param brandId Identifier of the Brand.
   * @return List of GetPriceResponseDto with the Prices that applies for the search.
   */
  public List<GetPriceResponseDto> execute(@NotNull String productId, @NotNull String brandId) {
    log.debug("execute: {}, {}", productId, brandId);

    List<Price> prices = priceService.findPrices(productId, brandId);

    return getPriceMapper.map(prices);
  }

}
