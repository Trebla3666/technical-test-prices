# Technical Test Prices

<p align="center">
<a href="#-summary">Summary</a>🔹
<a href="#-how-to-use">How to use</a>🔹  
<a href="#-technical-documentation">Technical documentation</a>🔹
<a href="#-contact">Contact</a>
</p>

## 📃 Summary

This project is a service responsible for manage product prices.

### 🔹 Technology

The main technology used in the project is:

    - Platform/language: Java 21
    - Maven: 3.9.6
    - Spring boot: 3.2.3
    - Database: H2
    - Type of artifact: Web service / REST endpoints

A complete list of used tecnhology is available in [Technology list](#-technology-list).

## 💫 ️Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### 🔹 Installation

To install the application execute in a terminal:
```
$ ./mvnw clean install
```
This will also execute all the tests available in the application.

### 🔹 Execution locally through Spring Boot

To run the application execute in a terminal:
```
$ ./mvnw spring-boot:run "-Dspring-boot.run.profiles=standalone"
```

### 🔹 Execution locally in a Docker image

To run the application execute in a terminal:
```
$ docker build -t test/prices .
$ docker run -p 8080:8080 test/prices
```

This command publish REST endpoints in the following urls:
```
http://localhost:8080/v1/brands/{brandId}/products/{productId}/applicable-price
http://localhost:8080/v1/brands/{brandId}/products/{productId}/prices
http://localhost:8080/v1/brands/{brandId}/products/{productId}/prices/{priceId}
```

## 🚀 How to use

The health check endpoint is available in the following url:
```
http://localhost:8080/actuator/health
```

Metrics can be exploiting by polling the following url:
```
http://localhost:8080/actuator/prometheus
```

The API definition is available in the following url:
```
http://localhost:8080/swagger-ui/index.html
```

The H2 console can be accessed through the following url:
```
http://localhost:8080/h2-console/

Access Data:
- url: jdbc:h2:mem:mydb
- username: h2-user
- password: h2-pass
```

A Postman collection with all endpoints is available in the root folder:
```
./Price API.postman_collection.json
```

## 👾 Technical documentation

### 🔹 Project architecture

A hexagonal architecture has been used as we have a clear domain, and we want to guarantee that the business logic is completely decoupled.

The different modules that represents the software architectures are:
- **domain**: Contains the DDD domain model and its 'needs' in the form of Services interfaces. It has no dependencies.
- **use-cases**: Contains the exposed functionalities so the ports to use them. Its only dependency is the domain module.
- **api-rest**: Exposes the functionality through REST endpoints. Its only dependency is the use-cases module.
- **infrastructure**: Provides the domain its needs in terms of storage by implementing the domain's Service interfaces. Its only dependency is the domain module.
- **boot**: Brings all the pieces from the other modules together and allows building and running the application.

### 🔹 Technology list
```
- Platform/language: Java 21
- Maven: 3.9.6
- Spring boot: 3.2.3
- Spring Framework
    - Spring Data JPA
    - Actuator
- Database: H2
- Lombok
- MapStructs
- OpenAPI generator
- Micrometer
    - Prometheus
    - Tracing Bridge Otel
- Springdoc Openapi
- Docker
- JUnit 5
- Mockito
```

### 🔹 ADRs

- API First approach has been followed providing the openapi specs prior the implementation.
- Application works internally in UTC and the dates information is stored as such.
- Logs includes traceId and spanId for distributed traceability.
- Metrics are available and can be easily exploited.
- Maven Wrapper is provided so users do not require local maven installation for its use.
- Testing has been done through Unit Tests and Integration Tests. End2End Tests are yet to be implemented as for now the domain is not complex enough.
- API Collection includes mere examples. Some parameters' values modifications might be required if the correct answer is desired.

## ✉️ Contact

Albert Gracía Martín: [trebla3666@hotmail.com](mailto:trebla3666@hotmail.com)

