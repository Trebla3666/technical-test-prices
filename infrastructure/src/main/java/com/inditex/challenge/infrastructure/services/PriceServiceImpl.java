package com.inditex.challenge.infrastructure.services;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.services.PriceService;
import com.inditex.challenge.infrastructure.mappers.PriceMapper;
import com.inditex.challenge.infrastructure.repositories.h2.PriceRepository;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Infrastructure implementation of the domain needs regarding the Price.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PriceServiceImpl implements PriceService {

  private final PriceRepository priceRepository;

  private final PriceMapper priceMapper;

  @Override
  public Optional<Price> findApplicablePrice(Instant applicableDate, String productId,
      String brandId) {
    log.debug("findApplicablePrices: {}, {}, {}", applicableDate, productId, brandId);
    return priceRepository.findApplicablePrice(productId, brandId, applicableDate)
        .map(priceMapper::map);
  }

  @Override
  public Optional<Price> findPrice(String publicId, String productId, String brandId) {
    log.debug("findPrice: {}, {}, {}", publicId, productId, brandId);
    return priceRepository.findByPublicIdAndProductIdAndBrandId(publicId, productId, brandId)
        .map(priceMapper::map);
  }

  @Override
  public List<Price> findPrices(String productId, String brandId) {
    log.debug("findPrices: {}, {}", productId, brandId);
    return priceMapper.map(priceRepository.findByProductIdAndBrandId(productId, brandId));
  }

  @Override
  public void updatePrice(Price price, String productId, String brandId) {
    log.debug("updatePrice: {}, {}, {}", price, productId, brandId);
    priceRepository.save(priceMapper.map(price, productId, brandId));
  }
}
