package com.inditex.challenge.infrastructure.services;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.Product;
import com.inditex.challenge.domain.services.ProductService;
import com.inditex.challenge.infrastructure.mappers.PriceMapper;
import com.inditex.challenge.infrastructure.repositories.h2.PriceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Infrastructure implementation of the domain needs regarding the Product.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

  private final PriceRepository priceRepository;

  private final PriceMapper priceMapper;

  @Override
  public void addPrice(Product product, Price price) {
    log.debug("addPrice: {}, {}", product, price);
    priceRepository.save(priceMapper.map(price, product.getProductId(), product.getBrandId()));
  }
}
