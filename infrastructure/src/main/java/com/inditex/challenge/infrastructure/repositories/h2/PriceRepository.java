package com.inditex.challenge.infrastructure.repositories.h2;

import com.inditex.challenge.infrastructure.repositories.h2.entities.PriceEntity;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository to manage Price entity.
 */
@Repository
public interface PriceRepository extends CrudRepository<PriceEntity, Long> {

  Optional<PriceEntity> findByPublicIdAndProductIdAndBrandId(
      @Param(value = "publicId") String publicId, @Param(value = "productId") String productId,
      @Param(value = "brandId") String brandId);

  List<PriceEntity> findByProductIdAndBrandId(@Param(value = "productId") String productId,
      @Param(value = "brandId") String brandId);

  @Query("select pe from PriceEntity pe where pe.status <> 'DECOMMISSIONED' "
      + "and pe.productId = :productId and pe.brandId = :brandId "
      + "and :applicableDate between pe.startDate and pe.endDate "
      + "order by pe.priority desc limit 1")
  Optional<PriceEntity> findApplicablePrice(@Param(value = "productId") String productId,
      @Param(value = "brandId") String brandId,
      @Param(value = "applicableDate") Instant applicableDate);
}
