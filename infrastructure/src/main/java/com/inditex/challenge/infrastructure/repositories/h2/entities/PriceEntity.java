package com.inditex.challenge.infrastructure.repositories.h2.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Entity that represents an Apex Framework version to apply to a project.
 */
@Data
@Entity
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PriceEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "price_entity_generator")
  @SequenceGenerator(name = "price_entity_generator",
      sequenceName = "price_entity_seq", allocationSize = 1)
  private Long id;

  @NotNull
  private String publicId;

  @NotNull
  private String productId;

  @NotNull
  private String status;

  @NotNull
  private String brandId;

  @NotNull
  private String priceListId;

  @NotNull
  private Instant startDate;

  @NotNull
  private Instant endDate;

  private int priority;

  @NotNull
  private BigDecimal amount;

  @NotNull
  private String currency;
}
