package com.inditex.challenge.infrastructure.mappers;

import com.inditex.challenge.domain.model.Amount;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.infrastructure.repositories.h2.entities.PriceEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapping related to the Price domain entity.
 */
@Mapper
public interface PriceMapper {

  @Mapping(target = "amount", source = ".")
  Price map(PriceEntity priceEntity);

  List<Price> map(List<PriceEntity> priceEntityList);

  @Mapping(target = "amount", source = "price.amount.value")
  @Mapping(target = "currency", source = "price.amount.currency")
  PriceEntity map(Price price, String productId, String brandId);

  @Mapping(target = "value", source = "amount")
  Amount mapAmount(PriceEntity priceEntity);

}
