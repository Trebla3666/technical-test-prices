package com.inditex.challenge.infrastructure.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.infrastructure.mappers.PriceMapper;
import com.inditex.challenge.infrastructure.repositories.h2.PriceRepository;
import com.inditex.challenge.infrastructure.repositories.h2.entities.PriceEntity;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PriceServiceImplTest {

  @Mock
  PriceRepository priceRepository;

  @Mock
  PriceMapper priceMapper;

  @InjectMocks
  PriceServiceImpl service;

  @Test
  void find_by_public_id_found() {
    // Given
    String publicId = "a_public_id";
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    PriceEntity found = PriceEntity.builder().build();
    when(priceRepository.findByPublicIdAndProductIdAndBrandId(publicId, productId, brandId))
        .thenReturn(Optional.of(found));
    Price mapped = Price.builder().build();
    when(priceMapper.map(found)).thenReturn(mapped);

    // When
    Optional<Price> price = service.findPrice(publicId, productId, brandId);

    // Then
    assertThat(price).isNotEmpty();
    assertThat(price.get()).isEqualTo(mapped);
  }

  @Test
  void find_by_public_id_not_found() {
    // Given
    String publicId = "a_public_id";
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    when(priceRepository.findByPublicIdAndProductIdAndBrandId(publicId, productId, brandId))
        .thenReturn(Optional.empty());

    // When
    Optional<Price> price = service.findPrice(publicId, productId, brandId);

    // Then
    assertThat(price).isEmpty();
  }

  @Test
  void find_prices_found() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    List<PriceEntity> found = List.of(PriceEntity.builder().build());
    when(priceRepository.findByProductIdAndBrandId(productId, brandId)).thenReturn(found);
    List<Price> mapped = List.of(Price.builder().build());
    when(priceMapper.map(found)).thenReturn(mapped);

    // When
    List<Price> price = service.findPrices(productId, brandId);

    // Then
    assertThat(price).isEqualTo(mapped);
  }

  @Test
  void find_prices_not_found() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    List<PriceEntity> found = Collections.emptyList();
    when(priceRepository.findByProductIdAndBrandId(productId, brandId)).thenReturn(found);
    List<Price> mapped = Collections.emptyList();
    when(priceMapper.map(found)).thenReturn(mapped);

    // When
    List<Price> price = service.findPrices(productId, brandId);

    // Then
    assertThat(price).isEqualTo(mapped);
  }

  @Test
  void find_applicable_prices_found() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    Instant applicableDate = Instant.ofEpochSecond(10);
    PriceEntity found = PriceEntity.builder().build();
    when(priceRepository.findApplicablePrice(productId, brandId, applicableDate))
        .thenReturn(Optional.of(found));
    Price mapped = Price.builder().build();
    when(priceMapper.map(found)).thenReturn(mapped);

    // When
    Optional<Price> price = service.findApplicablePrice(applicableDate, productId, brandId);

    // Then
    assertThat(price).isNotEmpty();
    assertThat(price.get()).isEqualTo(mapped);
  }

  @Test
  void find_applicable_prices_not_found() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    Instant applicableDate = Instant.ofEpochSecond(10);
    when(priceRepository.findApplicablePrice(productId, brandId, applicableDate))
        .thenReturn(Optional.empty());

    // When
    Optional<Price> price = service.findApplicablePrice(applicableDate, productId, brandId);

    // Then
    assertThat(price).isEmpty();
  }

  @Test
  void update_price() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    Price price = Price.builder().build();

    PriceEntity mapped = PriceEntity.builder().build();
    when(priceMapper.map(price, productId, brandId)).thenReturn(mapped);

    // When
    service.updatePrice(price, productId, brandId);

    // Then
    verify(priceRepository).save(mapped);
  }
}
