package com.inditex.challenge.infrastructure.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import com.inditex.challenge.domain.model.Amount;
import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.PriceStatus;
import com.inditex.challenge.infrastructure.repositories.h2.entities.PriceEntity;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class PriceMapperTest {

  PriceMapper mapper = Mappers.getMapper(PriceMapper.class);

  @Test
  void price_entity_to_price() {
    // Given
    PriceEntity origin = priceEntity(1L).build();

    // When
    final Price target = mapper.map(origin);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(price(1L).build());
  }

  @Test
  void price_entity_list_to_price_list() {
    // Given
    List<PriceEntity> origin = List.of(priceEntity(1L).build(), priceEntity(2L).build());

    // When
    final List<Price> target = mapper.map(origin);

    // Then
    assertThat(target).isNotNull();
    assertThat(target).hasSize(2);
  }

  @Test
  void price_to_price_entity() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    Price origin = price(1L).build();

    // When
    final PriceEntity target = mapper.map(origin, productId, brandId);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(priceEntity(1L).build());
  }

  @Test
  void price_product_to_price_entity() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    Price origin = price(1L).build();

    // When
    final PriceEntity target = mapper.map(origin, productId, brandId);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(priceEntity(1L).build());
  }

  private PriceEntity.PriceEntityBuilder priceEntity(Long id) {
    return PriceEntity.builder()
        .id(id)
        .publicId("a_public_id")
        .productId("a_product_id")
        .brandId("a_brand_id")
        .priceListId("a_price_list_id")
        .status("APPLICABLE")
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(new BigDecimal("10.75"))
        .currency("EUR");
  }

  private Price.PriceBuilder price(Long id) {
    return Price.builder()
        .id(id)
        .publicId("a_public_id")
        .priceListId("a_price_list_id")
        .status(PriceStatus.APPLICABLE)
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(Amount.builder()
            .value(new BigDecimal("10.75"))
            .currency("EUR")
            .build());
  }

}
