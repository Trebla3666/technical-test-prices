package com.inditex.challenge.infrastructure.services;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.inditex.challenge.domain.model.Price;
import com.inditex.challenge.domain.model.Product;
import com.inditex.challenge.infrastructure.mappers.PriceMapper;
import com.inditex.challenge.infrastructure.repositories.h2.PriceRepository;
import com.inditex.challenge.infrastructure.repositories.h2.entities.PriceEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {

  @Mock
  PriceRepository priceRepository;

  @Mock
  PriceMapper priceMapper;

  @InjectMocks
  ProductServiceImpl service;

  @Test
  void add_price() {
    // Given
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    Product product = Product.builder().productId(productId).brandId(brandId).build();
    Price price = Price.builder().build();

    PriceEntity priceEntity = PriceEntity.builder().build();
    when(priceMapper.map(price, productId, brandId)).thenReturn(priceEntity);

    // When
    service.addPrice(product, price);

    // Then
    verify(priceRepository).save(priceEntity);
  }
}
