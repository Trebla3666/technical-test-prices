package com.inditex.challenge;

import static java.util.Map.entry;
import static java.util.Map.ofEntries;
import static org.assertj.core.api.Assertions.assertThat;

import com.inditex.challenge.api.rest.model.Amount;
import com.inditex.challenge.api.rest.model.ApplicablePrice;
import com.inditex.challenge.api.rest.model.Price;
import com.inditex.challenge.api.rest.model.PriceInfo;
import com.inditex.challenge.infrastructure.repositories.h2.PriceRepository;
import com.inditex.challenge.infrastructure.repositories.h2.entities.PriceEntity;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.Optional;
import lombok.SneakyThrows;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(classes = {Application.class},
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"standalone", "test"})
@TestMethodOrder(OrderAnnotation.class)
public class PriceControllerIT {

  public static final String BRAND_ID = "1";

  public static final String PRODUCT_ID = "35455";

  @Autowired
  protected WebTestClient webTestClient;

  @Autowired
  private PriceRepository priceRepository;

  @Test
  @Order(1)
  public void get_applicable_price_test_1() {
    // When
    final ApplicablePrice response = webTestClient.get()
        .uri(uriBuilder -> uriBuilder
            .path("/v1/brands/{brandId}/products/{productId}/applicable-price")
            .queryParam("applicableDate", "2020-06-14T10:00:00.00Z")
            .build(BRAND_ID, PRODUCT_ID))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.OK)
        .expectBody(ApplicablePrice.class).returnResult().getResponseBody();

    //Then
    final ApplicablePrice expected = getApplicablePrice("1", "2020-06-14T00:00:00.00Z",
        "2020-12-31T23:59:59.00Z", "35.50");

    assertThat(response)
        .usingRecursiveComparison()
        .usingOverriddenEquals()
        .isEqualTo(expected);
  }

  @Test
  @Order(2)
  public void get_applicable_price_test_2() {
    // When
    final ApplicablePrice response = webTestClient.get()
        .uri(uriBuilder -> uriBuilder
            .path("/v1/brands/{brandId}/products/{productId}/applicable-price")
            .queryParam("applicableDate", "2020-06-14T16:00:00.00Z")
            .build(BRAND_ID, PRODUCT_ID))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.OK)
        .expectBody(ApplicablePrice.class).returnResult().getResponseBody();

    //Then
    final ApplicablePrice expected = getApplicablePrice("2", "2020-06-14T15:00:00.00Z",
        "2020-06-14T18:30:00.00Z", "25.45");

    assertThat(response)
        .usingRecursiveComparison()
        .usingOverriddenEquals()
        .isEqualTo(expected);
  }

  @Test
  @Order(3)
  public void get_applicable_price_test_3() {
    // When
    final ApplicablePrice response = webTestClient.get()
        .uri(uriBuilder -> uriBuilder
            .path("/v1/brands/{brandId}/products/{productId}/applicable-price")
            .queryParam("applicableDate", "2020-06-14T21:00:00.00Z")
            .build(BRAND_ID, PRODUCT_ID))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.OK)
        .expectBody(ApplicablePrice.class).returnResult().getResponseBody();

    //Then
    final ApplicablePrice expected = getApplicablePrice("1", "2020-06-14T00:00:00.00Z",
        "2020-12-31T23:59:59.00Z", "35.50");

    assertThat(response)
        .usingRecursiveComparison()
        .usingOverriddenEquals()
        .isEqualTo(expected);
  }

  @Test
  @Order(4)
  public void get_applicable_price_test_4() {
    // When
    final ApplicablePrice response = webTestClient.get()
        .uri(uriBuilder -> uriBuilder
            .path("/v1/brands/{brandId}/products/{productId}/applicable-price")
            .queryParam("applicableDate", "2020-06-15T10:00:00.00Z")
            .build(BRAND_ID, PRODUCT_ID))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.OK)
        .expectBody(ApplicablePrice.class).returnResult().getResponseBody();

    //Then
    final ApplicablePrice expected = getApplicablePrice("3", "2020-06-15T00:00:00.00Z",
        "2020-06-15T11:00:00.00Z", "30.50");

    assertThat(response)
        .usingRecursiveComparison()
        .usingOverriddenEquals()
        .isEqualTo(expected);
  }

  @Test
  @Order(5)
  public void get_applicable_price_test_5() {
    // When
    final ApplicablePrice response = webTestClient.get()
        .uri(uriBuilder -> uriBuilder
            .path("/v1/brands/{brandId}/products/{productId}/applicable-price")
            .queryParam("applicableDate", "2020-06-16T21:00:00.00Z")
            .build(BRAND_ID, PRODUCT_ID))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.OK)
        .expectBody(ApplicablePrice.class).returnResult().getResponseBody();

    //Then
    final ApplicablePrice expected = getApplicablePrice("4", "2020-06-15T16:00:00.00Z",
        "2020-12-31T23:59:59.00Z", "38.95");

    assertThat(response)
        .usingRecursiveComparison()
        .usingOverriddenEquals()
        .isEqualTo(expected);
  }

  @Test
  @Order(6)
  public void create_price() {
    // Given
    final PriceInfo priceInfo = new PriceInfo()
        .price(new Amount(BigDecimal.valueOf(49.99), "EUR"))
        .priceListId("1")
        .priority(0)
        .startDate(OffsetDateTime.parse("2020-06-20T09:00:00.00Z"))
        .endDate(OffsetDateTime.parse("2020-06-20T20:00:00.00Z"));

    // When
    final Price response = webTestClient.post()
        .uri("/v1/brands/{brandId}/products/{productId}/prices",
            ofEntries(entry("brandId", BRAND_ID), entry("productId", PRODUCT_ID)))
        .bodyValue(priceInfo)
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.CREATED)
        .expectBody(Price.class).returnResult().getResponseBody();

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getPublicId()).isNotNull();
    assertThat(response.getStatus()).isEqualTo(Price.StatusEnum.APPLICABLE);
    assertThat(response.getPrice()).isEqualTo(priceInfo.getPrice());
    assertThat(response.getPriceListId()).isEqualTo(priceInfo.getPriceListId());
    assertThat(response.getEndDate()).isEqualTo(priceInfo.getEndDate());
    assertThat(response.getStartDate()).isEqualTo(priceInfo.getStartDate());
    assertThat(response.getPriority()).isEqualTo(priceInfo.getPriority());
  }

  @Test
  @Order(7)
  @SneakyThrows
  @Sql("/sql/get_prices.sql")
  public void get_prices() {

    final String expected = new String(
        Files.readAllBytes(Paths.get("src/test/resources/json/get-prices-response.json")));

    // When
    webTestClient.get()
        .uri("/v1/brands/{brandId}/products/{productId}/prices",
            ofEntries(entry("brandId", BRAND_ID), entry("productId", PRODUCT_ID)))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.OK)
        .expectBody()
        .json(expected);

  }

  @Test
  @Order(8)
  @SneakyThrows
  @Sql("/sql/get_price.sql")
  public void get_price() {
    // Given
    String priceId = "to_be_retrieved";

    final String expected = new String(
        Files.readAllBytes(Paths.get("src/test/resources/json/get-price-response.json")));

    // When
    webTestClient.get()
        .uri("/v1/brands/{brandId}/products/{productId}/prices/{priceId}",
            ofEntries(entry("brandId", BRAND_ID),
                entry("productId", PRODUCT_ID),
                entry("priceId", priceId)))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.OK)
        .expectBody()
        .json(expected);
  }

  @Test
  @Order(9)
  @Sql("/sql/update_price.sql")
  public void update_price() {
    // Given
    String priceId = "to_be_updated";
    final PriceInfo priceInfo = new PriceInfo()
        .price(new Amount(BigDecimal.valueOf(49.99), "EUR"))
        .priceListId("2")
        .priority(5)
        .startDate(OffsetDateTime.parse("2020-06-20T09:00:00.00Z"))
        .endDate(OffsetDateTime.parse("2020-06-20T20:00:00.00Z"));

    // When
    webTestClient.put()
        .uri("/v1/brands/{brandId}/products/{productId}/prices/{priceId}",
            ofEntries(entry("brandId", BRAND_ID),
                entry("productId", PRODUCT_ID),
                entry("priceId", priceId)))
        .bodyValue(priceInfo)
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.NO_CONTENT);

    // Then
    Optional<PriceEntity> optUpdated = priceRepository.findByPublicIdAndProductIdAndBrandId(
        priceId, PRODUCT_ID, BRAND_ID);
    assertThat(optUpdated).isNotEmpty();
    PriceEntity update = optUpdated.get();
    assertThat(update.getAmount()).isEqualTo(priceInfo.getPrice().getValue());
    assertThat(update.getCurrency()).isEqualTo(priceInfo.getPrice().getCurrency());
    assertThat(update.getPriceListId()).isEqualTo(priceInfo.getPriceListId());
    assertThat(update.getPriority()).isEqualTo(priceInfo.getPriority());
    assertThat(update.getStartDate())
        .isEqualTo(Instant.ofEpochSecond(priceInfo.getStartDate().toEpochSecond()));
    assertThat(update.getEndDate())
        .isEqualTo(Instant.ofEpochSecond(priceInfo.getEndDate().toEpochSecond()));
  }

  @Test
  @Order(10)
  @Sql("/sql/delete_price.sql")
  public void delete_price() {
    // Given
    String priceId = "to_be_deleted";

    // When
    webTestClient.delete()
        .uri("/v1/brands/{brandId}/products/{productId}/prices/{priceId}",
            ofEntries(entry("brandId", BRAND_ID),
                entry("productId", PRODUCT_ID),
                entry("priceId", priceId)))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.NO_CONTENT);

    // Then
    Optional<PriceEntity> optUpdated = priceRepository.findByPublicIdAndProductIdAndBrandId(
        priceId, PRODUCT_ID, BRAND_ID);
    assertThat(optUpdated).isNotEmpty();
    assertThat(optUpdated.get().getStatus()).isEqualTo("DECOMMISSIONED");
  }

  private ApplicablePrice getApplicablePrice(final String priceList, final String startDate,
      final String endDate, final String amount) {
    return new ApplicablePrice()
        .productId(PRODUCT_ID)
        .brandId(BRAND_ID)
        .priceListId(priceList)
        .startDate(OffsetDateTime.parse(startDate))
        .endDate(OffsetDateTime.parse(endDate))
        .price(new Amount().value(new BigDecimal(amount)).currency("EUR"));
  }

}
