INSERT INTO PRICE_ENTITY
(ID, PUBLIC_ID, PRODUCT_ID, BRAND_ID, STATUS, PRICE_LIST_ID, START_DATE, END_DATE, PRIORITY, AMOUNT, CURRENCY)
VALUES 
(nextval('price_entity_seq'), random_uuid(), 35455, 1, 'APPLICABLE', 1, '2020-06-14 00:00:00+00:00', '2020-12-31 23:59:59+00:00', 0, 35.50, 'EUR'),
(nextval('price_entity_seq'), random_uuid(), 35455, 1, 'APPLICABLE', 2, '2020-06-14 15:00:00+00:00', '2020-06-14 18:30:00+00:00', 1, 25.45, 'EUR'),
(nextval('price_entity_seq'), random_uuid(), 35455, 1, 'APPLICABLE', 3, '2020-06-15 00:00:00+00:00', '2020-06-15 11:00:00+00:00', 1, 30.50, 'EUR'),
(nextval('price_entity_seq'), random_uuid(), 35455, 1, 'APPLICABLE', 4, '2020-06-15 16:00:00+00:00', '2020-12-31 23:59:59+00:00', 1, 38.95, 'EUR');