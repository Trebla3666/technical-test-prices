package com.inditex.challenge.api.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.inditex.challenge.api.rest.mappers.ErrorMapper;
import com.inditex.challenge.api.rest.model.Error;
import com.inditex.challenge.domain.exceptions.AlreadyDecommissionedException;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import java.lang.reflect.Executable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;

@ExtendWith(MockitoExtension.class)
public class PriceControllerAdviceTest {

  @Mock
  ErrorMapper errorMapper;

  @InjectMocks
  PriceControllerAdvice priceControllerAdvice;

  @Test
  void conflict() {
    // Given
    AlreadyDecommissionedException exception = new AlreadyDecommissionedException("test");
    Error error = new Error();
    when(errorMapper.map(exception)).thenReturn(error);

    // When
    ResponseEntity<Error> response = priceControllerAdvice.handleException(exception);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    assertThat(response.getBody()).isEqualTo(error);
  }

  @Test
  void not_found() {
    // Given
    NotFoundException exception = new NotFoundException("test");
    Error error = new Error();
    when(errorMapper.map(exception)).thenReturn(error);

    // When
    ResponseEntity<Error> response = priceControllerAdvice.handleException(exception);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(error);
  }

  @Test
  void bad_request() {
    // Given
    MethodArgumentNotValidException exception = mock(MethodArgumentNotValidException.class);
    Error error = new Error();
    when(errorMapper.map(exception)).thenReturn(error);

    // When
    ResponseEntity<Error> response = priceControllerAdvice.handleException(exception);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(error);
  }

  @Test
  void unsupported_media_type() {
    // Given
    HttpMediaTypeNotSupportedException exception = new HttpMediaTypeNotSupportedException("test");
    Error error = new Error();
    when(errorMapper.map(exception)).thenReturn(error);

    // When
    ResponseEntity<Error> response = priceControllerAdvice.handleException(exception);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    assertThat(response.getBody()).isEqualTo(error);
  }

  @Test
  void internal_server_error() {
    // Given
    Exception exception = new Exception("test");
    Error error = new Error();
    when(errorMapper.map(exception)).thenReturn(error);

    // When
    ResponseEntity<Error> response = priceControllerAdvice.handleException(exception);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(response.getBody()).isEqualTo(error);
  }
}
