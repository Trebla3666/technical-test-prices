package com.inditex.challenge.api.rest.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import com.inditex.challenge.api.rest.model.Amount;
import com.inditex.challenge.api.rest.model.ApplicablePrice;
import com.inditex.challenge.api.rest.model.Price;
import com.inditex.challenge.api.rest.model.Price.StatusEnum;
import com.inditex.challenge.api.rest.model.PriceInfo;
import com.inditex.challenge.api.rest.model.Prices;
import com.inditex.challenge.use.cases.price.dtos.CreateRequestDto;
import com.inditex.challenge.use.cases.price.dtos.CreateResponseDto;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.dtos.UpdateRequestDto;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class PriceControllerMapperTest {

  PriceControllerMapper mapper = Mappers.getMapper(PriceControllerMapper.class);

  @Test
  void create_rest_request_to_use_case_request() {
    // Given
    String brandId = "a_brand_id";
    String productId = "a_product_id";
    PriceInfo priceInfo = priceInfo();

    // When
    final CreateRequestDto target = mapper.map(brandId, productId, priceInfo);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(createRequestDto().build());
  }

  @Test
  void create_use_case_response_to_rest_response() {
    // Given
    CreateResponseDto createResponseDto = createResponseDto().build();
    PriceInfo priceInfo = priceInfo();

    // When
    final Price target = mapper.map(createResponseDto, priceInfo);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(price());
  }

  @Test
  void applicable_price_use_case_response_to_rest_response() {
    // Given
    String brandId = "a_brand_id";
    String productId = "a_product_id";
    GetPriceResponseDto getPriceResponseDto = getPriceResponseDto().build();

    // When
    final ApplicablePrice target = mapper.map(brandId, productId, getPriceResponseDto);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(applicablePrice());
  }

  @Test
  void prices_use_case_response_to_rest_response() {
    // Given
    List<GetPriceResponseDto> getPriceResponseDtoList = List.of(getPriceResponseDto().build());

    // When
    final Prices target = mapper.map(getPriceResponseDtoList);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(prices());
  }

  @Test
  void price_use_case_response_to_rest_response() {
    // Given
    GetPriceResponseDto getPriceResponseDto = getPriceResponseDto().build();

    // When
    final Price target = mapper.map(getPriceResponseDto);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(price());
  }

  @Test
  void price_info_to_update_use_case_request() {
    // Given
    String priceId = "a_price_id";
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    PriceInfo priceInfo = priceInfo();

    // When
    final UpdateRequestDto target = mapper.map(priceInfo, priceId, productId, brandId);

    // Then
    assertThat(target)
        .usingRecursiveComparison()
        .ignoringAllOverriddenEquals()
        .isEqualTo(updateRequestDto().build());
  }

  private PriceInfo priceInfo() {
    return new PriceInfo("a_price_list_id",
        OffsetDateTime.ofInstant(Instant.ofEpochSecond(10), ZoneOffset.UTC),
        OffsetDateTime.ofInstant(Instant.ofEpochSecond(20), ZoneOffset.UTC),
        new Amount(new BigDecimal("10.75"), "EUR"),
        5
    );
  }

  private Price price() {
    return new Price("a_price_list_id",
        OffsetDateTime.ofInstant(Instant.ofEpochSecond(10), ZoneOffset.UTC),
        OffsetDateTime.ofInstant(Instant.ofEpochSecond(20), ZoneOffset.UTC),
        new Amount(new BigDecimal("10.75"), "EUR"),
        5,
        "a_public_id",
        StatusEnum.APPLICABLE
    );
  }

  private Prices prices() {
    return new Prices(List.of(price()));
  }

  private ApplicablePrice applicablePrice() {
    return new ApplicablePrice("a_product_id",
        "a_brand_id",
        "a_price_list_id",
        OffsetDateTime.ofInstant(Instant.ofEpochSecond(10), ZoneOffset.UTC),
        OffsetDateTime.ofInstant(Instant.ofEpochSecond(20), ZoneOffset.UTC),
        new Amount(new BigDecimal("10.75"), "EUR")
    );
  }

  private CreateRequestDto.CreateRequestDtoBuilder createRequestDto() {
    return CreateRequestDto.builder()
        .productId("a_product_id")
        .brandId("a_brand_id")
        .priceListId("a_price_list_id")
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(new BigDecimal("10.75"))
        .currency("EUR");
  }

  private CreateResponseDto.CreateResponseDtoBuilder createResponseDto() {
    return CreateResponseDto.builder()
        .publicId("a_public_id")
        .status("APPLICABLE");
  }

  private GetPriceResponseDto.GetPriceResponseDtoBuilder getPriceResponseDto() {
    return GetPriceResponseDto.builder()
        .publicId("a_public_id")
        .status("APPLICABLE")
        .priceListId("a_price_list_id")
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(new BigDecimal("10.75"))
        .currency("EUR");
  }

  private UpdateRequestDto.UpdateRequestDtoBuilder updateRequestDto() {
    return UpdateRequestDto.builder()
        .productId("a_product_id")
        .brandId("a_brand_id")
        .priceId("a_price_id")
        .priceListId("a_price_list_id")
        .priority(5)
        .startDate(Instant.ofEpochSecond(10))
        .endDate(Instant.ofEpochSecond(20))
        .amount(new BigDecimal("10.75"))
        .currency("EUR");
  }


}
