package com.inditex.challenge.api.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.inditex.challenge.api.rest.mappers.PriceControllerMapper;
import com.inditex.challenge.api.rest.model.ApplicablePrice;
import com.inditex.challenge.api.rest.model.Price;
import com.inditex.challenge.api.rest.model.PriceInfo;
import com.inditex.challenge.api.rest.model.Prices;
import com.inditex.challenge.use.cases.price.CreateUseCase;
import com.inditex.challenge.use.cases.price.DeleteUseCase;
import com.inditex.challenge.use.cases.price.GetApplicablePriceUseCase;
import com.inditex.challenge.use.cases.price.GetPriceUseCase;
import com.inditex.challenge.use.cases.price.GetPricesUseCase;
import com.inditex.challenge.use.cases.price.UpdateUseCase;
import com.inditex.challenge.use.cases.price.dtos.CreateRequestDto;
import com.inditex.challenge.use.cases.price.dtos.CreateResponseDto;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.dtos.UpdateRequestDto;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class PriceControllerTest {

  @Mock
  CreateUseCase createUseCase;

  @Mock
  GetApplicablePriceUseCase applicablePriceUseCase;

  @Mock
  GetPricesUseCase pricesUseCase;

  @Mock
  DeleteUseCase deleteUseCase;

  @Mock
  GetPriceUseCase getPriceUseCase;

  @Mock
  UpdateUseCase updateUseCase;

  @Mock
  PriceControllerMapper priceRestMapper;

  @InjectMocks
  PriceController priceController;

  @Test
  void create_price() {
    // Given
    String brandId = "a_brand_id";
    String productId = "a_product_id";
    PriceInfo priceInfo = new PriceInfo();
    CreateRequestDto requestDto = CreateRequestDto.builder().build();
    when(priceRestMapper.map(brandId, productId, priceInfo)).thenReturn(requestDto);

    CreateResponseDto responseDto = CreateResponseDto.builder().build();
    when(createUseCase.execute(requestDto)).thenReturn(responseDto);

    Price price = new Price();
    when(priceRestMapper.map(responseDto, priceInfo)).thenReturn(price);

    // When
    ResponseEntity<Price> response = priceController.createPrice(brandId, productId, priceInfo);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    assertThat(response.getBody()).isEqualTo(price);
  }

  @Test
  void applicable_price() {
    // Given
    String brandId = "a_brand_id";
    String productId = "a_product_id";
    Instant applicableInstant = Instant.ofEpochSecond(10);
    OffsetDateTime applicableDate = OffsetDateTime.ofInstant(applicableInstant, ZoneOffset.UTC);
    when(priceRestMapper.map(applicableDate)).thenReturn(applicableInstant);

    GetPriceResponseDto responseDto = GetPriceResponseDto.builder().build();
    when(applicablePriceUseCase.execute(productId, brandId, applicableInstant))
        .thenReturn(responseDto);

    ApplicablePrice applicablePrice = new ApplicablePrice();
    when(priceRestMapper.map(brandId, productId, responseDto)).thenReturn(applicablePrice);

    // When
    ResponseEntity<ApplicablePrice> response = priceController.getApplicablePrice(brandId,
        productId, applicableDate);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(applicablePrice);
  }

  @Test
  void prices() {
    // Given
    String brandId = "a_brand_id";
    String productId = "a_product_id";
    List<GetPriceResponseDto> responseDtoList = List.of(GetPriceResponseDto.builder().build());
    when(pricesUseCase.execute(productId, brandId)).thenReturn(responseDtoList);

    Prices prices = new Prices();
    when(priceRestMapper.map(responseDtoList)).thenReturn(prices);

    // When
    ResponseEntity<Prices> response = priceController.getPrices(brandId, productId);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(prices);
  }

  @Test
  void delete_price() {
    // Given
    String priceId = "a_price_id";
    String productId = "a_product_id";
    String brandId = "a_brand_id";

    // When
    ResponseEntity<Void> response = priceController.deletePrice(priceId, productId, brandId);

    // Then
    verify(deleteUseCase).execute(brandId, productId, priceId);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
  }

  @Test
  void price() {
    // Given
    String priceId = "a_price_id";
    String productId = "a_product_id";
    String brandId = "a_brand_id";
    GetPriceResponseDto getPriceResponseDto = GetPriceResponseDto.builder().build();
    when(getPriceUseCase.execute(priceId, productId, brandId)).thenReturn(getPriceResponseDto);

    Price price = new Price();
    when(priceRestMapper.map(getPriceResponseDto)).thenReturn(price);

    // When
    ResponseEntity<Price> response = priceController.getPrice(brandId, productId, priceId);

    // Then
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(price);
  }

  @Test
  void update_price() {
    // Given
    String brandId = "a_brand_id";
    String productId = "a_product_id";
    String priceId = "a_price_id";
    PriceInfo priceInfo = new PriceInfo();
    UpdateRequestDto updateRequestDto = UpdateRequestDto.builder().build();
    when(priceRestMapper.map(priceInfo, priceId, productId, brandId)).thenReturn(updateRequestDto);

    // When
    ResponseEntity<Void> response = priceController.updatePrice(brandId, productId, priceId,
        priceInfo);

    // Then
    verify(updateUseCase).execute(updateRequestDto);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
  }

}
