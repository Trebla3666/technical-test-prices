package com.inditex.challenge.api.rest;

import com.inditex.challenge.api.rest.mappers.ErrorMapper;
import com.inditex.challenge.api.rest.model.Error;
import com.inditex.challenge.domain.exceptions.AlreadyDecommissionedException;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Advice to handle the possible exceptions when operating Fraud Rules.
 */
@Slf4j
@ControllerAdvice(basePackageClasses = {PriceController.class})
@RequiredArgsConstructor
public class PriceControllerAdvice {

  private final ErrorMapper errorMapper;

  @ExceptionHandler(AlreadyDecommissionedException.class)
  public ResponseEntity<Error> handleException(final AlreadyDecommissionedException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMapper.map(e));
  }

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<Error> handleException(final NotFoundException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMapper.map(e));
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Error> handleException(final MethodArgumentNotValidException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMapper.map(e));
  }

  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  public ResponseEntity<Error> handleException(final HttpMediaTypeNotSupportedException e) {
    log.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(errorMapper.map(e));
  }

  @ExceptionHandler(Throwable.class)
  public ResponseEntity<Error> handleException(final Throwable t) {
    log.error(t.getMessage(), t);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMapper.map(t));
  }

}
