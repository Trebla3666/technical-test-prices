package com.inditex.challenge.api.rest.mappers;

import com.inditex.challenge.api.rest.model.Amount;
import com.inditex.challenge.api.rest.model.ApplicablePrice;
import com.inditex.challenge.api.rest.model.Price;
import com.inditex.challenge.api.rest.model.PriceInfo;
import com.inditex.challenge.api.rest.model.Prices;
import com.inditex.challenge.use.cases.price.dtos.CreateRequestDto;
import com.inditex.challenge.use.cases.price.dtos.CreateResponseDto;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.dtos.UpdateRequestDto;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapping related to the Price Rest Controller.
 */
@Mapper
public interface PriceControllerMapper {

  @Mapping(target = "priceListId", source = "priceInfo.priceListId")
  @Mapping(target = "startDate", source = "priceInfo.startDate")
  @Mapping(target = "endDate", source = "priceInfo.endDate")
  @Mapping(target = "priority", source = "priceInfo.priority")
  @Mapping(target = "amount", source = "priceInfo.price.value")
  @Mapping(target = "currency", source = "priceInfo.price.currency")
  CreateRequestDto map(String brandId, String productId, PriceInfo priceInfo);

  @Mapping(target = "publicId", source = "responseDto.publicId")
  @Mapping(target = "status", source = "responseDto.status")
  @Mapping(target = "priceListId", source = "priceInfo.priceListId")
  @Mapping(target = "startDate", source = "priceInfo.startDate")
  @Mapping(target = "endDate", source = "priceInfo.endDate")
  @Mapping(target = "priority", source = "priceInfo.priority")
  @Mapping(target = "price", source = "priceInfo.price")
  Price map(CreateResponseDto responseDto, PriceInfo priceInfo);

  @Mapping(target = "priceListId", source = "dto.priceListId")
  @Mapping(target = "startDate", source = "dto.startDate")
  @Mapping(target = "endDate", source = "dto.endDate")
  @Mapping(target = "priority", source = "dto.priority")
  @Mapping(target = "publicId", source = "dto.publicId")
  @Mapping(target = "status", source = "dto.status")
  @Mapping(target = "price", source = "dto")
  Price map(GetPriceResponseDto dto);

  @Mapping(target = "priceListId", source = "dto.priceListId")
  @Mapping(target = "startDate", source = "dto.startDate")
  @Mapping(target = "endDate", source = "dto.endDate")
  @Mapping(target = "price", source = "dto")
  ApplicablePrice map(String brandId, String productId, GetPriceResponseDto dto);

  @Mapping(target = "priceListId", source = "priceInfo.priceListId")
  @Mapping(target = "startDate", source = "priceInfo.startDate")
  @Mapping(target = "endDate", source = "priceInfo.endDate")
  @Mapping(target = "priority", source = "priceInfo.priority")
  @Mapping(target = "amount", source = "priceInfo.price.value")
  @Mapping(target = "currency", source = "priceInfo.price.currency")
  UpdateRequestDto map(PriceInfo priceInfo, String priceId, String productId, String brandId);

  @Mapping(target = "value", source = "dto.amount")
  @Mapping(target = "currency", source = "dto.currency")
  Amount mapAmount(GetPriceResponseDto dto);

  default Prices map(List<GetPriceResponseDto> dtoList) {
    return new Prices(dtoList.stream()
        .map(this::map)
        .collect(Collectors.toList()));
  }

  default OffsetDateTime map(Instant instant) {
    return OffsetDateTime.ofInstant(instant, ZoneOffset.UTC);
  }

  default Instant map(OffsetDateTime offsetDateTime) {
    return Instant.ofEpochSecond(offsetDateTime.toEpochSecond());
  }

}
