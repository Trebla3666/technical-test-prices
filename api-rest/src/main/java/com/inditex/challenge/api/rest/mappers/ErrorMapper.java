package com.inditex.challenge.api.rest.mappers;

import com.inditex.challenge.api.rest.model.Error;
import com.inditex.challenge.domain.exceptions.AlreadyDecommissionedException;
import com.inditex.challenge.use.cases.price.exceptions.NotFoundException;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;

@Mapper
public interface ErrorMapper {

  @Mapping(target = "code", constant = "PUC-AD-001")
  @Mapping(target = "title", constant = "price_already_decommissioned")
  @Mapping(target = "description", source = "message")
  Error map(AlreadyDecommissionedException exception);

  @Mapping(target = "code", constant = "PUC-NF-001")
  @Mapping(target = "title", constant = "price_not_found")
  @Mapping(target = "description", source = "message")
  Error map(NotFoundException exception);

  @Mapping(target = "code", constant = "AUC-BR-001")
  @Mapping(target = "title", constant = "bad_request")
  @Mapping(target = "description", source = ".")
  Error map(MethodArgumentNotValidException exception);

  @Mapping(target = "code", constant = "AUC-BR-002")
  @Mapping(target = "title", constant = "unsupported_media_type")
  @Mapping(target = "description", source = "message")
  Error map(HttpMediaTypeNotSupportedException exception);

  @Mapping(target = "code", constant = "AUC-UE-001")
  @Mapping(target = "title", constant = "unexpected_error")
  @Mapping(target = "description", constant = "An unexpected error occurred")
  Error map(Throwable exception);

  default String mapDescription(MethodArgumentNotValidException exception) {
    return Optional.of(exception.getBindingResult())
        .map(BindingResult::getFieldErrors)
        .orElse(Collections.emptyList())
        .stream()
        .map(fieldError -> String.format("%s %s",
            fieldError.getField(), fieldError.getDefaultMessage()))
        .collect(Collectors.joining(", "));
  }

}
