package com.inditex.challenge.api.rest;

import com.inditex.challenge.api.rest.mappers.PriceControllerMapper;
import com.inditex.challenge.api.rest.model.ApplicablePrice;
import com.inditex.challenge.api.rest.model.Price;
import com.inditex.challenge.api.rest.model.PriceInfo;
import com.inditex.challenge.api.rest.model.Prices;
import com.inditex.challenge.use.cases.price.CreateUseCase;
import com.inditex.challenge.use.cases.price.DeleteUseCase;
import com.inditex.challenge.use.cases.price.GetApplicablePriceUseCase;
import com.inditex.challenge.use.cases.price.GetPriceUseCase;
import com.inditex.challenge.use.cases.price.GetPricesUseCase;
import com.inditex.challenge.use.cases.price.UpdateUseCase;
import com.inditex.challenge.use.cases.price.dtos.CreateRequestDto;
import com.inditex.challenge.use.cases.price.dtos.CreateResponseDto;
import com.inditex.challenge.use.cases.price.dtos.GetPriceResponseDto;
import com.inditex.challenge.use.cases.price.dtos.UpdateRequestDto;
import java.time.OffsetDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class PriceController implements PriceApi {

  private final CreateUseCase createUseCase;

  private final GetApplicablePriceUseCase applicablePriceUseCase;

  private final GetPricesUseCase pricesUseCase;

  private final DeleteUseCase deleteUseCase;

  private final GetPriceUseCase getPriceUseCase;

  private final UpdateUseCase updateUseCase;

  private final PriceControllerMapper priceRestMapper;

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<Price> createPrice(String brandId, String productId, PriceInfo priceInfo) {
    log.debug("createPrice: {}, {}, {}", brandId, productId, priceInfo);

    CreateRequestDto requestDto = priceRestMapper.map(brandId, productId, priceInfo);
    CreateResponseDto responseDto = createUseCase.execute(requestDto);
    Price price = priceRestMapper.map(responseDto, priceInfo);

    return ResponseEntity.status(HttpStatus.CREATED).body(price);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<ApplicablePrice> getApplicablePrice(String brandId, String productId,
      OffsetDateTime applicableDate) {
    log.debug("getApplicablePrice: {}, {}, {}", brandId, productId, applicableDate);

    GetPriceResponseDto responseDto = applicablePriceUseCase.execute(productId, brandId,
        priceRestMapper.map(applicableDate));
    ApplicablePrice applicablePrice = priceRestMapper.map(brandId, productId, responseDto);

    return ResponseEntity.ok(applicablePrice);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<Prices> getPrices(String brandId, String productId) {
    log.debug("getPrices: {}, {}", brandId, productId);

    List<GetPriceResponseDto> getPriceResponseDtoList = pricesUseCase.execute(productId, brandId);
    Prices prices = priceRestMapper.map(getPriceResponseDtoList);

    return ResponseEntity.ok(prices);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<Void> deletePrice(String brandId, String productId, String priceId) {
    log.debug("deletePrice: {}", priceId);
    deleteUseCase.execute(priceId, productId, brandId);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<Price> getPrice(String brandId, String productId, String priceId) {
    log.debug("getPrice: {}", priceId);
    GetPriceResponseDto responseDto = getPriceUseCase.execute(priceId, productId, brandId);
    Price price = priceRestMapper.map(responseDto);
    return ResponseEntity.ok(price);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ResponseEntity<Void> updatePrice(String brandId, String productId, String priceId,
      PriceInfo priceInfo) {
    log.debug("updatePrice: {}, {}", priceId, priceInfo);
    UpdateRequestDto requestDto = priceRestMapper.map(priceInfo, priceId, productId, brandId);
    updateUseCase.execute(requestDto);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }
}
